# Digit

Provides the type `Digit`, which represents a decimal digit, i.e. a
number from 0 to 9. By having a digit as a type we gain a guarantee
that a value is within that range without any need for validation.

## Example usage
A ZIP code (a US postal code) is an example of a type that can benefit
from using `Digit` in its representation:

    type ZIP
        = ZIP Digit Digit Digit Digit Digit

This definition means that any value of type `ZIP` is guaranteed to be
a valid ZIP code, because it is impossible to construct an invalid
value.