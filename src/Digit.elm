module Digit exposing (Digit, fromInt, readDigits, toInt)

{-|

@docs Digit, fromInt, readDigit, toInt

-}

import Maybe.Extra exposing (combine)
import List exposing (foldr, map)
import String exposing (String)


{-| Representation of a decimal digit, i.e. a value from 0 to 9.
-}
type Digit
    = Zero
    | One
    | Two
    | Three
    | Four
    | Five
    | Six
    | Seven
    | Eight
    | Nine


{-| Gets the digit value as an Int.
-}
toInt : Digit -> Int
toInt d =
    case d of
        Zero ->
            0

        One ->
            1

        Two ->
            2

        Three ->
            3

        Four ->
            4

        Five ->
            5

        Six ->
            6

        Seven ->
            7

        Eight ->
            8

        Nine ->
            9


{-| Attempts to get the digit value from an Int.
-}
fromInt : Int -> Maybe Digit
fromInt x =
    if x == 0 then
        Just Zero

    else if x == 1 then
        Just One

    else if x == 2 then
        Just Two

    else if x == 3 then
        Just Three

    else if x == 4 then
        Just Four

    else if x == 5 then
        Just Five

    else if x == 6 then
        Just Six

    else if x == 7 then
        Just Seven

    else if x == 8 then
        Just Eight

    else if x == 9 then
        Just Nine

    else
        Nothing


{-| Attempts to get the digit value from a Char.
-}
fromChar : Char -> Maybe Digit
fromChar c =
    if c == '0' then
        Just Zero

    else if c == '1' then
        Just One

    else if c == '2' then
        Just Two

    else if c == '3' then
        Just Three

    else if c == '4' then
        Just Four

    else if c == '5' then
        Just Five

    else if c == '6' then
        Just Six

    else if c == '7' then
        Just Seven

    else if c == '8' then
        Just Eight

    else if c == '9' then
        Just Nine

    else
        Nothing


{-| Attempts to read multiple digits from a String.

    readDigits "1234" == Just [One, Two, Three, Four]

    -- no non-digit characters
    readDigits "a234" == Nothing

    -- no whitespace
    readDigits " 1234" == Nothing

    -- leading zeros are their own digits
    readDigits "09" == Just [Zero, Nine]

    -- empty string produces empty list (NOT Nothing)
    readDigits "" == Just []


-}
readDigits : String -> Maybe (List Digit)
readDigits str = combine <| map fromChar <| String.toList str